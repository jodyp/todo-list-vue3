# Todo List by Muhammad Adnand Jody Pratama

# Features :
1. Add new todos and store it to pinia.
2. Change todos's status to DONE.
3. Filter status ALL, TODO and DONE.

# How to run the app
After you clone from this repo, do the following steps :

```sh
npm install
```

```sh
npm run dev
```

