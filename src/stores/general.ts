import { ref } from 'vue'
import { defineStore } from 'pinia'

interface ITodo {
  id: number
  content: string
  status: boolean
}

export const useUtilsStore = defineStore('utils', () => {
  const expanded = ref(false)

  function expand() {
    expanded.value = !expanded.value
  }

  return { expanded, expand }
})

export const useTodoStore = defineStore('todo', () => {
  const todos = ref<ITodo[]>([
    { id: 1, content: 'Makan malam bersama keluarga', status: true },
    { id: 2, content: 'Berkebun bersama paman', status: false },
    { id: 3, content: 'Berenang dengan teman-teman kantor', status: false },
    { id: 4, content: 'Mengerjakan quiz akselerasi privy', status: true }
  ])

  const filteredTodos = ref<ITodo[]>([])

  function addTodo(todo: ITodo) {
    todos.value?.push(todo)
  }

  function finishTodo(id: number, status: boolean) {
    const indexChoosen = todos.value.findIndex((index) => index.id === id)
    todos.value[indexChoosen].status = !status
  }

  function filterTodo(status: boolean) {
    filteredTodos.value = todos.value.filter((index) => index.status === status)
  }

  function getAllTodo() {
    filteredTodos.value = todos.value
  }

  return { todos, filteredTodos, addTodo, finishTodo, filterTodo, getAllTodo }
})
